import { CursosService } from './cursos.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css'],
})
export class CursosComponent implements OnInit {
  nomePortal: string;
  cursos: string[];
  constructor(private CursosService: CursosService) {
    this.nomePortal = 'https://loiane.training/';

    //Usando o ngFor ele aplica a regra do for automatico
    // for (let i = 0; i < this.cursos.length; i++) {
    //   let curso = this.cursos[i];
    // }
    //var servico = new CursosService();
    this.cursos = this.CursosService.getCursos();
  }
  ngOnInit(): void {}
}
